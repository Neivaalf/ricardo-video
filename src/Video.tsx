import {Composition} from 'remotion';
import {Buy} from './Buy/Buy';

const FPS = 30;
const DURATION_IN_SEC = 6;

export const Video: React.FC = () => {
	return (
		<>
			<Composition
				id="Buy"
				component={Buy}
				durationInFrames={FPS * DURATION_IN_SEC}
				fps={FPS}
				width={1080}
				height={1920}
				defaultProps={{
					articleTitle: 'Spiderman PS5',
					price: '97.00 CHF',
					sellerName: 'Martha',
					imageUrl: 'https://img-dev.ricardostatic.ch/t_raw/pl/1000427462/1/1/',
				}}
			/>
		</>
	);
};
