import {spring} from 'remotion';
import {useCurrentFrame} from 'remotion';
import {useVideoConfig} from 'remotion';
import {Sequence} from 'remotion';

export const Seller: React.FC<{startTime: number; sellerName: string}> = ({
	startTime,
	sellerName,
}) => {
	const {fps} = useVideoConfig();
	const frame = useCurrentFrame();

	const transition = spring({
		frame: frame - startTime,
		from: 0,
		to: 1,
		fps,
		config: {
			stiffness: 100,
		},
	});
	return (
		<Sequence
			from={startTime}
			durationInFrames={Infinity}
			name="Thanks {sellerName}"
		>
			<div
				className="center"
				style={{
					top: '20.5em',
				}}
			>
				<span
					style={{
						display: 'block',
						whiteSpace: 'nowrap',
						transform: 'rotateZ(-5deg)',
					}}
				>
					<div
						style={{
							opacity: transition,
							transform: `translateY(${Math.abs(transition - 1) * 100}px)`,
						}}
					>
						Thanks <span className="highlight">{sellerName}</span> !
					</div>
				</span>
			</div>
		</Sequence>
	);
};
