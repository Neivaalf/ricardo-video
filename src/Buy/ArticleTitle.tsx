import {spring} from 'remotion';
import {useCurrentFrame} from 'remotion';
import {useVideoConfig} from 'remotion';
import {Sequence} from 'remotion';

export const ArticleTitle: React.FC<{startTime: number; articleTitle: string}> =
	({startTime, articleTitle}) => {
		const {fps} = useVideoConfig();
		const frame = useCurrentFrame();

		const transition = spring({
			frame: frame - startTime,
			from: 0,
			to: 1,
			fps,
			config: {
				stiffness: 100,
			},
		});

		return (
			<Sequence
				from={startTime}
				durationInFrames={Infinity}
				name="{articleTitle}"
			>
				<div
					className="highlight center"
					style={{
						top: '9.5em',
					}}
				>
					<span
						style={{
							display: 'block',
							fontSize: '1.5em',
							whiteSpace: 'nowrap',
							opacity: transition,
							transform: `translateY(${Math.abs(transition - 1) * 100}px)`,
						}}
					>
						{articleTitle}
					</span>
				</div>
			</Sequence>
		);
	};
