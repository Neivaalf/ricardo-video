import {spring} from 'remotion';
import {useCurrentFrame} from 'remotion';
import {useVideoConfig} from 'remotion';
import {Sequence} from 'remotion';

export const Price: React.FC<{startTime: number; price: string}> = ({
	startTime,
	price,
}) => {
	const {fps} = useVideoConfig();
	const frame = useCurrentFrame();

	const transition = spring({
		frame: frame - startTime,
		from: 0,
		to: 1,
		fps,
		config: {
			stiffness: 100,
		},
	});

	return (
		<Sequence from={startTime} durationInFrames={Infinity} name="for {price}">
			<div
				className="center"
				style={{
					top: '11.5em',
				}}
			>
				<div
					style={{
						whiteSpace: 'nowrap',
						opacity: transition,
						transform: `translateY(${Math.abs(transition - 1) * 100}px)`,
					}}
				>
					for <span className="highlight">{price}</span>
				</div>
			</div>
		</Sequence>
	);
};
