import {Img, Sequence, spring, useCurrentFrame, useVideoConfig} from 'remotion';

export const ArticleImage: React.FC<{startTime: number; imageUrl: string}> = ({
	startTime,
	imageUrl,
}) => {
	const {fps} = useVideoConfig();
	const frame = useCurrentFrame();

	const transition = spring({
		frame: frame - 2.5 * fps,
		from: 0,
		to: 1,
		fps,
		config: {
			stiffness: 100,
		},
	});

	return (
		<Sequence
			from={startTime}
			durationInFrames={Infinity}
			name="{articleImage}"
		>
			<div
				className="center"
				style={{
					top: '13.5em',
					opacity: frame < 2.5 * fps ? 0 : 1,
				}}
			>
				<Img
					src={imageUrl}
					style={{
						height: '6em',
						boxShadow: '5px 5px 13px 5px rgba(0,0,0,0.2)',
						opacity: transition,
						transform: `translateX(${Math.abs(transition - 1) * -100}px)`,
					}}
				/>
			</div>
		</Sequence>
	);
};
