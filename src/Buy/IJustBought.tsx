import {useCurrentFrame} from 'remotion';
import {spring} from 'remotion';
import {useVideoConfig} from 'remotion';
import {Sequence} from 'remotion';

export const IJustBought: React.FC<{startTime: number}> = ({startTime}) => {
	const {fps} = useVideoConfig();
	const frame = useCurrentFrame();

	const transition = spring({
		frame: frame - startTime,
		from: 0,
		to: 1,
		fps,
		config: {
			stiffness: 100,
		},
	});

	return (
		<Sequence
			from={startTime}
			durationInFrames={Infinity}
			name="I just bought..."
		>
			<div
				className="center"
				style={{
					top: '7.5em',
				}}
			>
				<div
					style={{
						opacity: transition,
						transform: `translateY(${Math.abs(transition - 1) * 100}px)`,
					}}
				>
					I just bought
				</div>
			</div>
		</Sequence>
	);
};
