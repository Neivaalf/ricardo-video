import {useCurrentFrame} from 'remotion'
import {useVideoConfig} from 'remotion'
import {spring} from 'remotion'
import {Sequence} from 'remotion'
export const Yay: React.FC<{startTime: number}> = ({startTime}) => {

	const {fps} = useVideoConfig()
	const frame = useCurrentFrame()

	const transition = spring({
		frame,
		from: 0,
		to: 1,
		fps,
		config: {
			stiffness: 200,
			damping: 15 
		}, 
	})

  return 		(
	<Sequence from={startTime} durationInFrames={Infinity} name="Yay !">

		<div
			className="center" style={{
      top: '1em',
			}}
		>
			<b style={{
				display: 'block',
				whiteSpace: 'nowrap',
				fontSize: '4em',
				transform: `rotateZ(${transition * -12}deg) translateY(${(Math.abs(transition - 1)) * -200}px)`,
				opacity: transition
			}}
			>
				Yay !
			</b> 
		</div>
	</Sequence>
)
}