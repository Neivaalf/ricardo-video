import {useCurrentFrame} from 'remotion';
import {Sequence, useVideoConfig} from 'remotion';
import {Img} from 'remotion';
import './font.css';
import logoRDot from './logoRDot.svg';

import {Yay} from './Yay';
import {IJustBought} from './IJustBought';
import {ArticleTitle} from './ArticleTitle';
import {Price} from './Price';
import {Seller} from './Seller';
import {ArticleImage} from './ArticleImage';

const COLORS = {
	WHITE: '#FFFFFF',
	ORANGE: '#EF7310',
};

export const Buy: React.FC<{
	articleTitle: string;
	price: string;
	sellerName: string;
	imageUrl: string;
}> = ({articleTitle, price, sellerName, imageUrl}) => {
	const {fps} = useVideoConfig();

	return (
		<div
			style={{
				fontFamily: 'Ubuntu, sans-serif',

				height: '100%',
				width: '100%',

				display: 'flex',
				flexDirection: 'column',

				justifyContent: 'center',
				alignItems: 'center',

				fontSize: '5em',

				backgroundColor: COLORS.ORANGE,
				color: COLORS.WHITE,
			}}
		>
			<Yay startTime={0} />
			<IJustBought startTime={Math.round(fps * 0.75)} />
			<ArticleTitle
				startTime={Math.round(fps * 1.5)}
				articleTitle={articleTitle}
			/>
			<Price startTime={Math.round(fps * 2)} price={price} />
			<ArticleImage startTime={0} imageUrl={imageUrl} />

			<Seller startTime={Math.round(fps * 3)} sellerName={sellerName} />

			<Img
				src={logoRDot}
				style={{
					height: '150px',
					position: 'absolute',
					bottom: '50px',
					right: '50px',
					transform: 'rotateZ(-12deg)',
				}}
			/>
		</div>
	);
};
